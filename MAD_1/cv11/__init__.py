import numpy as np
import pandas as ps


class ActorsAndMovies:
    def __init__(self, path):
        self.actors = ps.read_csv(
            path,
            skiprows=1,
            delim_whitespace=True,
            nrows=10,
            names=('n', 'name'),
            index_col='n',
        )
        self.movies = ps.read_csv(
            path,
            skiprows=11,
            delim_whitespace=True,
            nrows=24,
            names=('n', 'name'),
            index_col='n',
        )
        self.edges = ps.read_csv(
            path,
            skiprows=37,
            delim_whitespace=True,
            nrows=57,
            names=('actor', 'movie', 'weight'),
        )

    actor_offset = 1

    @classmethod
    def actor_i(cls, actor_n):
        return actor_n - cls.actor_offset

    @classmethod
    def actor_n(cls, actor_i):
        return actor_i + cls.actor_offset

    movie_offset = 11

    @classmethod
    def movie_i(cls, movie_n):
        return movie_n - cls.movie_offset

    @classmethod
    def movie_n(cls, movie_i):
        return movie_i + cls.movie_offset
