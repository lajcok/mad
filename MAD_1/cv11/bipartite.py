import numpy as np
import pandas as ps

from MAD_1.cv03.graph_as_list import Graph
from MAD_1.cv04 import closeness, diameter, avg_dist
from MAD_1.cv11 import ActorsAndMovies


def one_mode_projection(matrix):
    """
    One-mode projecting with simple weighting
    :param matrix: A matrix or 2D array
    :return: Double of matrices along each axis
    """

    def part(di):
        res_m = np.zeros((matrix.shape[di],) * 2, dtype=int)
        for i in range(len(res_m)):
            for j in range(i + 1):
                for k in range(matrix.shape[not di]):
                    res_m[i, j] += min(matrix[idx[di], idx[not di]] for idx in [(i, k), (j, k)])
                    res_m[j, i] = res_m[i, j]
        return res_m

    return tuple(map(part, range(len(matrix.shape))))


if __name__ == '__main__':
    aam = ActorsAndMovies('ActorsAndMovies.net')
    ai, mi = aam.actor_i, aam.movie_i
    an, mn = aam.actor_n, aam.movie_n

    bi_matrix = np.zeros((len(aam.actors), len(aam.movies)), dtype=int)
    for a, m, w in zip(aam.edges.actor, aam.edges.movie, aam.edges.weight):
        bi_matrix[ai(a), mi(m)] = w

    actors_matrix, movies_matrix = one_mode_projection(bi_matrix)

    ps.DataFrame(
        (an(i), an(j), actors_matrix[i, j])
        for i in range(len(actors_matrix))
        for j in range(i)
        if actors_matrix[i, j] is not 0
    ).to_csv('actors.csv', header=False, index=False)

    ps.DataFrame(
        (mn(i), mn(j), movies_matrix[i, j])
        for i in range(len(movies_matrix))
        for j in range(i)
        if movies_matrix[i, j] is not 0
    ).to_csv('movies.csv', header=False, index=False)

    print('Actors:')
    print(actors_matrix)
    actors_graph = Graph(
        nodes=[an(i) for i in range(len(actors_matrix))],
        edges=[
            (an(i), an(j))
            for i in range(len(actors_matrix))
            for j in range(i)
            if bool(actors_matrix[i, j])
        ]
    )
    for i in range(len(actors_matrix)):
        actors_matrix[i, i] = 0
    print('deg_dist:', actors_graph.degree())
    print('avg_deg:', actors_graph.avg_degree())
    print(f'diameter={diameter(actors_matrix)}; avg_dist={avg_dist(actors_matrix)}')
    for i in range(len(actors_matrix)):
        print(f'closeness#1{an(i)}={closeness(actors_matrix, i)}')

    # print('Movies:')
    # print(movies_matrix)
    # movies_graph = Graph(
    #     nodes=[an(i) for i in range(len(movies_matrix))],
    #     edges=[
    #         (an(i), an(j))
    #         for i in range(len(movies_matrix))
    #         for j in range(i)
    #         if bool(movies_matrix[i, j])
    #     ]
    # )
    # movies_matrix_alt = movies_matrix.copy()
    # for i in range(len(movies_matrix_alt)):
    #     movies_matrix_alt[i, i] = 0
    # print('deg_dist:', movies_graph.degree())
    # print('avg_deg:', movies_graph.avg_degree())
    # print(f'diameter={diameter(movies_matrix_alt)}; avg_dist={avg_dist(movies_matrix_alt)}')
    # for i in range(len(movies_matrix_alt)):
    #     print(f'closeness#1{an(i)}={closeness(movies_matrix_alt, i)}')
