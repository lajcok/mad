import tkinter as tk
from time import time
from tkinter import ttk
from tkinter import filedialog

import pandas as ps
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm

from MAD_1.cv06.agglomerative import Agglomerative
from MAD_1.proj_implementation.data import load, preprocess


class Gui:
    class Loader:
        def __init__(self, gui, menu):
            self.gui = gui
            self.menu = menu
            self.menu.add_command(label='Open', command=self.open, accelerator='Ctrl+O')
            gui.root.bind_all('<Control-o>', gui.handle(self.open))

        def open(self):
            path = filedialog.askopenfilename(
                title='Select dataset',
                filetypes=[
                    ('CSV dataset', '*.csv'),
                    ('Any format', '*.*'),
                ]
            )
            if path:
                self.gui.load(path)

    class DatasetOptions:
        def __init__(self, master):
            self.master = master
            self.data = None

            self.inst_n = tk.IntVar(value=0)
            inst_frame = tk.Frame(master)
            tk.Label(inst_frame, text='Instances:').pack(side=tk.LEFT)
            tk.Label(inst_frame, textvariable=self.inst_n).pack(side=tk.LEFT)
            inst_frame.pack()

            lim_frame = tk.Frame(master)
            tk.Label(lim_frame, text='Limit:').pack(side=tk.LEFT)
            self.inst_limit = tk.IntVar(value=0)
            tk.Entry(lim_frame, textvariable=self.inst_limit, width=5).pack(side=tk.LEFT)
            lim_frame.pack()

            self.attr_frame = tk.LabelFrame(master, text='Attributes')
            self.attr_frame.pack()
            self.attr_checkboxes = {}
            self.attr_vars = {}

        def set_data(self, data):
            self.data = data
            self.inst_n.set(len(data))
            self.inst_limit.set(len(data))

            for check in self.attr_checkboxes.values():
                check.pack_forget()
            self.attr_checkboxes = {}

            for attr in data:
                self.attr_vars[attr] = tk.BooleanVar(value=False)
                self.attr_checkboxes[attr] = tk.Checkbutton(
                    self.attr_frame,
                    text=str(attr),
                    variable=self.attr_vars[attr]
                )
                self.attr_checkboxes[attr].pack(anchor='w')

    def __init__(self, root):
        self.root = root

        # Menu
        self.main_menu = tk.Menu(self.root)
        self.root.config(menu=self.main_menu)

        self.file_menu = tk.Menu(self.main_menu, tearoff=0)
        self.main_menu.add_cascade(label='File', menu=self.file_menu)

        self.data_menu = tk.Menu(self.main_menu, tearoff=0)
        self.main_menu.add_cascade(label='Data', menu=self.data_menu)
        self.data_menu.add_command(label='Preview', command=self.data_preview)

        self.loader = self.Loader(self, self.file_menu)
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Exit', command=self.root.quit, accelerator='Ctrl+Q')
        self.root.bind_all('<Control-q>', self.handle(self.root.quit))

        # Left
        left_frame = tk.Frame(root)
        left_frame.pack(side=tk.LEFT)

        # Overview
        data_frame = tk.LabelFrame(left_frame, text='Dataset')
        data_frame.pack()
        self.data_options = self.DatasetOptions(data_frame)

        self.do_not_stop = tk.BooleanVar(value=False)
        tk.Checkbutton(left_frame, text='Run until end', variable=self.do_not_stop).pack(fill=tk.X)

        self.run_btn = tk.Button(left_frame, text='Run', command=self.run_clustering, state=tk.DISABLED)
        self.run_btn.pack(fill=tk.X)

        self.sse_btn = tk.Button(left_frame, text='Analyze SSE', command=self.view_sse_evolution)
        self.sse_btn.pack(pady=5)

        self.prev_btn = tk.Button(left_frame, text='Preview 2D', command=self.preview_clusters)
        self.prev_btn.pack()

        # Results view
        self.storage = []
        self.tree = ttk.Treeview(self.root)
        self.tree['columns'] = 'clusters', 'sse'
        self.tree.column('#0')
        self.tree.column('clusters')
        self.tree.column('sse')
        self.tree.heading('#0', text='#', anchor=tk.W)
        self.tree.heading('clusters', text='Clusters', anchor=tk.W)
        self.tree.heading('sse', text='SSE', anchor=tk.W)
        self.tree.pack(expand=True, fill=tk.BOTH)
        self.tree.bind('<Double-1>', self.preview_clusters)

        self.data = None

    def handle(self, action):
        def _handler(*args, **kwargs):
            return action()

        return _handler

    def load(self, path):
        self.data = preprocess(load(path))
        self.data_options.set_data(self.data)
        self.run_btn.config(state=tk.NORMAL)

    def run_clustering(self):
        self.storage = []
        self.tree.delete(*self.tree.get_children())
        limit = self.data_options.inst_limit.get()

        start = time()
        attrs = [
            attr
            for attr, enabled in self.data_options.attr_vars.items()
            if enabled.get()
        ]
        nodes = list(zip(*[self.data[attr][:limit] for attr in attrs]))
        agg = Agglomerative(
            nodes=nodes,
            stopper=Agglomerative.stopper_variance() if not self.do_not_stop.get() else Agglomerative.stopper_full(),
        )
        while agg:
            print(f'agg#{len(self.storage)}')
            self.storage.append(agg)
            agg = agg.up()

        dur = time() - start

        for i, step in enumerate(self.storage):
            self.tree.insert('', i, str(i), text=str(i), values=(len(step.clusters), step.sse()))

        f = open('log.txt', 'w')
        f.write(f'Instances total: {len(self.data)}\n')
        f.write(f'Instances processed: {len(nodes)}\n')
        f.write(f'Dimension: {len(attrs)}\n')
        f.write(f'Duration: {dur} s\n')
        f.write('\nProgress:\n')
        for i, step in enumerate(self.storage):
            f.write(f'agg{i}: n={len(step.clusters)}, sse={step.sse()}\n')
        f.close()

        ps.DataFrame(
            [
                (i, *node)
                for i, cluster in enumerate(self.storage[len(self.storage) - 1].clusters)
                for node in cluster.nodes
            ], columns=['cluster'] + [f'x{i + 1}' for i in range(len(attrs))]
        ).to_csv('out.csv')

    def view_sse_evolution(self):
        plt.title('SSE evolution during Agglomerative Hierarchical Clustering')
        plt.xlabel('n of clusters')
        plt.ylabel('SSE')
        plt.scatter(
            x=range(len(self.storage)),
            y=[agg.sse() for agg in self.storage],
            marker='.',
        )
        plt.show()

    def preview_clusters(self, *args, **kwargs):
        sel = self.tree.selection()
        if sel:
            i = int(sel[0])
            self.show_clusters(self.storage[i])

    @staticmethod
    def show_clusters(agg):
        plt.title('Clusters 2D preview')
        plt.xlabel('x')
        plt.ylabel('y')
        colors = cm.jet(np.linspace(0, 1, len(agg.clusters)))
        for (i, cluster), color in zip(enumerate(agg.clusters), colors):
            plt.scatter(
                x=[pt[0] for pt in cluster.nodes],
                y=[pt[1] for pt in cluster.nodes],
                color=color,
                marker='.',
            )
        plt.show()

    def data_preview(self):
        data_win = tk.Toplevel(self.root)
        tree = ttk.Treeview(data_win)
        cols = [c for c in self.data]
        tree['columns'] = cols
        for col in cols:
            tree.heading(col, text=col, anchor=tk.W)
        for i, t in enumerate(zip(*[self.data[c] for c in cols])):
            tree.insert('', i, str(i), text=str(i), values=t)
        tree.pack(expand=True, fill=tk.BOTH)


if __name__ == '__main__':
    _root = tk.Tk()
    _gui = Gui(_root)
    _root.mainloop()
