import pandas as ps
from pandas.api.types import is_string_dtype


def preprocess(data):
    def _preprocess(col):
        if is_string_dtype(data[col]):
            return {
                f'{col}.{category}': [
                    val == category
                    for val in data[col]
                ]
                for category in data[col].unique()
            }
        else:
            return {col: data[col]}

    # Drop rows with any empty cells
    data.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)

    df = ps.DataFrame({
        name: values
        for col in data
        for name, values in _preprocess(col).items()
    }, index=data.index)

    return df


load = ps.read_csv

if __name__ == '__main__':
    _data = ps.read_csv('Iris.csv')
    _prep = preprocess(_data)
