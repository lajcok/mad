import numpy as np
import pandas as ps

TYPE_NUM = 1
TYPE_CAT = 2


def generate_data(instances, dimension, types=None):
    def _generate(tp):
        if tp == TYPE_CAT:
            return np.random.choice(['a', 'b', 'c'], size=instances)
        else:
            return np.random.uniform(0, 500, size=instances)

    types = types or (TYPE_NUM for _ in range(dimension))
    return ps.DataFrame({
        f'x{dim + 1}': _generate(tp)
        for dim, tp in enumerate(types)
    })


if __name__ == '__main__':
    _test = generate_data(1000, 5, [TYPE_NUM] * 3 + [TYPE_CAT] * 2)
    _test.to_csv('Generated.csv')
