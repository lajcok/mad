import os
import sys
from math import sqrt

import pandas as pd
import matplotlib.pyplot as plt


# Dot product
def dot(a, b):
    return sum(map(lambda t: t[0] * t[1], zip(a, b)))


# Euclidean norm
def norm(a):
    return sqrt(dot(a, a))


# Euclidean distance
def dist(a, b):
    return norm(tuple(map(lambda t: t[0] - t[1], zip(a, b))))


# Cosine similarity
def sim(a, b):
    return dot(a, b) / (norm(a) * norm(b))


if __name__ == '__main__':
    column = sys.argv[1] if 1 in sys.argv else 'sepal'

    length = f'{column}_length'
    width = f'{column}_width'

    base_path = os.path.dirname(__file__)
    data = pd.read_csv(os.path.join(base_path, 'data/iris.csv'), sep=';', decimal=',')

    n = len(data.index)
    if not n:
        print('No rows loaded')
        exit(1)

    # averages and variances
    mean = (
        sum(data[length]) / n,
        sum(data[width]) / n,
    )
    sepal = list(zip(data[length], data[width]))
    var = sum(map(lambda a: dist(a, mean) ** 2, sepal)) / n
    print(f'mean = {mean}')
    print(f'var = {var}')

    # cosine similarity
    cosine = sim(sepal[0], sepal[1])
    print(f'cos = {cosine}')

    # scatter plots
    plt.scatter(data[length], data[width])
    plt.scatter(data[length][:2], data[width][:2])
    plt.scatter(mean[0], mean[1])
    plt.show()
