import sys

from MAD_1.cv05 import load_dataset, mean, dist, variance, OutputData, normal_distribution, output_data, histogram

if __name__ == '__main__':
    col = sys.argv[1] if 1 in sys.argv else 'sepal'
    data = load_dataset()

    length = data[f'{col}_length']
    width = data[f'{col}_width']

    leaves = list(zip(length, width))
    print(f'leaves: {leaves}')

    u = (mean(length), mean(width))
    print(f'mean = {u}')

    distances = [
        round(dist(leaf, u) * 10) / 10
        for leaf in leaves
    ]
    print(f'distances: {distances}')

    dist_u = mean(distances)
    dist_o2 = variance(distances, dist_u)
    print(f'u={dist_u}, o2={dist_o2}')

    basic_data = [
        (dst, len([d for d in distances if d == dst]))
        for dst in sorted(set(distances))
    ]
    complete_data = [
        OutputData(
            value=dst,
            count=cnt,
            relative=cnt / len(distances),
            normal=normal_distribution(dst, dist_u, dist_o2),
        )
        for dst, cnt in basic_data
    ]

    output = output_data(complete_data)

    print('\nstats:')
    print(output)

    histogram(distances)
