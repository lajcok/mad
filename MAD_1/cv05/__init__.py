import math
import os
from collections import namedtuple

import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame


def load_dataset():
    base_path = os.path.dirname(__file__)
    return pd.read_csv(os.path.join(base_path, '../cv02/data/iris.csv'), sep=';', decimal=',')


# Mean
def mean(xs):
    return sum(xs) / len(xs)


# Dot product
def dot(a, b):
    return sum(map(lambda t: t[0] * t[1], zip(a, b)))


# Euclidean norm
def norm(a):
    return dot(a, a) ** (1 / 2)


# Euclidean distance
def dist(a, b):
    return norm(tuple(map(lambda t: t[0] - t[1], zip(a, b))))


# Variance
def variance(xs, u):
    return sum(map(
        lambda x: dist((x,), (u,)) ** 2,
        xs
    )) / len(xs)


def normal_distribution(x, u, o2):
    return (2 * math.pi * o2) ** (-1 / 2) * math.exp(-((x - u) ** 2) / (2 * o2))


OutputData = namedtuple('OutputData', ('value', 'count', 'relative', 'normal'))


def output_data(data):
    output = DataFrame(data)
    output.to_csv('data.csv', header=('value', 'count', 'relative', 'normal'))
    output.to_clipboard()
    return output


def histogram(values):
    plt.hist(
        values,
        bins=[
            val / 10 + .05
            for val in range(round(min(values) * 10), round(max(values) * 10) + 1)
        ],
        rwidth=.6,
        # density=True,  # TODO How to convert to probabilities?
    )
    plt.show()
