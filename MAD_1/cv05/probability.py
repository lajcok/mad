import sys

from MAD_1.cv05 import load_dataset, mean, normal_distribution, histogram, output_data, variance, OutputData

# Mean

if __name__ == '__main__':
    col = sys.argv[1] if 1 in sys.argv else 'sepal_length'

    data = load_dataset()

    c = [
        (v, len([l for l in data[col] if l == v]))
        for v in sorted(set(data[col]))
    ]

    u = mean(data[col])
    o2 = variance(data[col], u)
    print(f'u={u}, o2={o2}')

    complete = [
        OutputData(v, l, l / len(data), normal_distribution(v, u, o2))
        for v, l in c
    ]

    output = output_data(complete)

    print('\nstats:')
    print(output)

    histogram(data[col])
