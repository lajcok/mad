import os
from functools import reduce

import pandas as ps
import matplotlib.pyplot as plt

from MAD_1.cv03.graph_as_list import Graph as GraphBase


class Graph(GraphBase):
    def neighbors(self, node, include_self=False):
        return {
            n
            for e in filter(lambda e: node in e, self.edges)
            for n in e
            if include_self or n != node
        }

    def induced(self, nodes):
        return Graph(
            nodes=nodes,
            edges=[
                e for e in self.edges
                if len(e) == len([n for n in e if n in nodes])
            ],
        )

    def clustering_coefficient(self, node=None):
        def ci(n):
            if n not in self.nodes:
                raise ValueError('Given node does not exist in the graph')
            gi = self.induced(self.neighbors(n))
            ni = len(gi.nodes)
            mi = len(gi.edges)
            return (2 * mi) / (ni * (ni - 1)) if ni > 1 else 0

        nodes = node if type(node) is list else [node] if node else self.nodes
        return sum([ci(n) for n in nodes]) / len(nodes)

    def transitivity(self):
        return self.clustering_coefficient()

    def clustering_effect(self, degree):
        return self.clustering_coefficient(self.get_by_degree(degree))


if __name__ == '__main__':
    base_dir = os.path.dirname(__file__)
    data = ps.read_csv(os.path.join(base_dir, '..', 'cv03', 'KarateClub.csv'), sep=';', header=None)
    g = Graph(
        nodes=set(reduce(
            lambda a, b: a + b,
            [list(data[c]) for c in data]
        )),
        edges=set(zip(
            *tuple(
                list(data[c])
                for c in data
            ))
        ),
    )
    print(g)

    print('Coefficients')
    for nd in g.nodes:
        print(f'node#{nd}\tCC={g.clustering_coefficient(nd)}')

    print(f'\nTransitivity = {g.transitivity()}')

    print(f'\tEffect')
    degrees = sorted(g.degree())
    ces = [g.clustering_effect(deg) for deg in degrees]
    for deg, ce in zip(degrees, ces):
        print(f'deg#{deg}\tCE={ce}')

    plt.xlabel('Degree')
    plt.ylabel('Clustering Effect')
    plt.scatter(x=degrees, y=ces)
    plt.show()
