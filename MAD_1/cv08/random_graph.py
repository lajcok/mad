import sys
from functools import reduce
from random import random

import numpy as np

from MAD_1.cv07.clustering_coefficient import Graph as GraphBase
from MAD_1.cv04 import Graph as GraphMatrix


class Graph(GraphBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._clusters = None

    def clusters(self):
        if self._clusters is None:
            subsets = [{node} for node in self.nodes]
            for edge in self.edges:
                matched = []
                rest = []

                for subset in subsets:
                    found = False
                    for node in edge:
                        if node in subset:
                            found = True
                            break
                    (matched if found else rest).append(subset)

                cluster = set(reduce(lambda a, b: a + b, (map(list, matched)), [*edge]))
                subsets = [cluster] + rest

            self._clusters = [
                self.induced(subset)
                for subset in subsets
            ]

        return self._clusters


def as_matrix(graph):
    nds = list(graph.nodes)
    node_dict = {
        prev: cur
        for prev, cur in zip(nds, range(len(graph.nodes)))
    }
    m = np.zeros((len(nds),) * 2, dtype=int)
    for a, b in [[node_dict[n] for n in edge] for edge in graph.edges]:
        m[a, b] = m[b, a] = 1
    return GraphMatrix(m)


def random_graph(n, p):
    nodes = set(range(n))
    edges = set()
    tried = set()

    for a in nodes:
        for b in nodes:
            if a is b:
                continue

            tried.add((a, b))
            if (b, a) not in tried and random() < p:
                edges.add((a, b))

    return Graph(nodes, edges)


def visualize(graph):
    import matplotlib.pyplot as plt
    import matplotlib.lines as lns

    plt.axis('off')
    ax = plt.gca()

    nds = list(graph.nodes)
    n = len(nds)
    angle = 2 * np.pi / n
    vls = angle * np.arange(n)
    x, y = np.cos(vls), np.sin(vls)

    plt.scatter(x, y, c='xkcd:red')

    for a, b in graph.edges:
        i, j = nds.index(a), nds.index(b)
        ax.add_line(lns.Line2D((x[i], x[j]), (y[i], y[j])))

    for i, lbl in enumerate(nds):
        ax.annotate(lbl, (x[i], y[i]))

    plt.show()


if __name__ == '__main__':
    if len(sys.argv) >= 3:
        ns = int(sys.argv[1])
        ps = [float(arg) for arg in sys.argv[2:]]
    else:
        ns = 550
        ps = [1e-3, 1.82e-3, 3e-3]

    gs = [random_graph(ns, p) for p in ps]

    for g in gs:
        print(f'n={len(g.nodes)}')
        print(f'e={len(g.edges)}')
        print(f'avg_deg={g.avg_degree()}')
        print(f'cluster_c={g.clustering_coefficient()}')
        for c in g.clusters():
            cm = as_matrix(c)
            print(f'cluster{c.nodes}, n={len(c.nodes)}:')
            print(f'\tdiameter={cm.diameter()}, avg_dist={cm.avg_dist()}, cluster_c={c.clustering_coefficient()}')

    visualize(gs[0])
