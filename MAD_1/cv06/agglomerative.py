from functools import reduce

from matplotlib import pyplot as plt

from MAD_1.cv06 import dist, load_dataset, mean, variance


class Agglomerative:
    class Cluster:
        def __init__(self, nodes, cache=None):
            self.nodes = nodes
            self.cache = cache or {}
            self._centroid = None

        def centroid(self):
            if not self._centroid:
                self._centroid = mean(self.nodes)
            return self._centroid

        '''
        single-link clustering
        '''

        def __sub__(self, other):
            if other not in self.cache:
                self.cache[other] = min([
                    dist(a, b)
                    for a in self.nodes
                    for b in other.nodes
                ])

            return self.cache[other]

        def __add__(self, other):
            return Agglomerative.Cluster(
                nodes=self.nodes + other.nodes,
                cache={
                    k: min(self.cache[k], other.cache[k]) if k in other.cache else self.cache[k]
                    for k in self.cache
                }
            )

        def __str__(self):
            return str(self.nodes)

    @staticmethod
    def stopper_full():
        return lambda a, latest: len(a.clusters) <= 1

    @staticmethod
    def stopper_variance():
        def stopper_implementation(a, latest):
            if not latest:
                return True
            total = reduce(lambda c, b: c + b, [c for c in a.clusters])
            return variance(latest.nodes, latest.centroid()) >= variance(total.nodes, total.centroid()) / 4
        return stopper_implementation

    def __init__(self, nodes=None, clusters=None, stopper=None):
        self.clusters = clusters or [
            Agglomerative.Cluster([node])
            for node in nodes
        ]
        self.stopper = stopper or self.stopper_full()

    def up(self):
        pairs = [
            (x, y)
            for x in self.clusters
            for y in self.clusters
            if x is not y
        ]

        closest = None
        for (a, b) in pairs:
            if not closest or a - b < closest[0] - closest[1]:
                closest = (a, b)

        merged = closest[0] + closest[1] if closest else None

        if self.stopper(self, merged):
            return None

        return Agglomerative(
            clusters=[merged] + [
                c for c in self.clusters
                if c not in closest
            ],
            stopper=self.stopper,
        )

    def sse(self):
        return sum([
            sum([
                dist(node, cluster.centroid()) ** 2
                for node in cluster.nodes
            ])
            for cluster in self.clusters
        ])

    def __str__(self):
        return ''.join([str(c) for c in self.clusters])


if __name__ == '__main__':
    data = load_dataset(300)
    leaves = list(zip(*[
        data[col]
        for col in data
        # if col.startswith('sepal') or col.startswith('petal')
    ]))

    agg = Agglomerative(nodes=leaves, stopper=Agglomerative.stopper_variance())
    i = 0
    sse_storage = []
    while agg:
        latest_cluster = agg.clusters[0]
        print(f'agg#{i}: n={len(agg.clusters)};\tsse={agg.sse()};\tvar={variance(latest_cluster.nodes, latest_cluster.centroid())}')
        sse_storage.append((len(agg.clusters), agg.sse()))
        agg = agg.up()
        i += 1

    plt.title('SSE evolution during Agglomerative Hierarchical Clustering')
    plt.xlabel('n of clusters')
    plt.ylabel('SSE')
    plt.scatter(
        x=[s[0] for s in sse_storage],
        y=[s[1] for s in sse_storage],
        marker='.',
    )
    plt.show()
