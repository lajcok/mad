import os
import pandas as pd


# Numeric

# Dot product
def dot(a, b):
    return sum(map(lambda t: t[0] * t[1], zip(a, b)))


# Euclidean norm
def norm(a):
    return dot(a, a) ** (1 / 2)


# Euclidean distance
def dist(a, b):
    return norm(tuple(map(lambda t: t[0] - t[1], zip(a, b))))


# Variance
def variance(xs, u):
    return sum(map(
        lambda x: dist(tuple(x), tuple(u)) ** 2,
        xs
    )) / len(xs)


# Mean
def mean(xs):
    def _mean(_xs):
        return sum(_xs) / len(_xs)

    return tuple([
        _mean([xs[i][dim] for i in range(len(xs))])
        for dim in range(len(xs[0]))
    ]) if len(xs) and type(xs[0]) is tuple else _mean(xs)


# Data

def load_dataset(n=None):
    base_path = os.path.dirname(__file__)
    # return pd.read_csv(os.path.join(base_path, '../cv02/data/iris.csv'), sep=';', decimal=',', nrows=n)
    return pd.read_csv(os.path.join(base_path, '../proj_implementation/Generated.csv'), nrows=n, usecols=(1,2))
