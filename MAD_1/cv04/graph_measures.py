import os

import numpy as np
import pandas as ps

from MAD_1.cv03.graph_as_matrix import Graph as GraphBase


def floyd_warshall(matrix):
    n = len(matrix)

    dist = np.empty((n, n))
    for i in range(n):
        for j in range(n):
            dist[i, j] = matrix[i, j] or np.inf if i != j else 0

    for k in range(n):
        for i in range(n):
            for j in range(n):
                d = dist[i, k] + dist[k, j]
                if d < dist[i, j]:
                    dist[i, j] = d

    return dist


def avg_dist(dist_matrix):
    n = len(dist_matrix)
    return (2 / (n * (n - 1))) * sum(
        dist_matrix[i, j]
        for i in range(n)
        for j in range(i + 1, n)
    ) if n > 1 else 0


def diameter(dist_matrix):
    n = len(dist_matrix)
    return max(
        dist_matrix[i, j]
        for i in range(n)
        for j in range(i + 1, n)
    ) if n > 1 else 0


def closeness(dist_matrix, v):
    return len(dist_matrix[v]) / sum(dist_matrix[v])


class Graph(GraphBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dm = None

    def distance_matrix(self):
        if self.dm is None:
            self.dm = floyd_warshall(self.matrix)
        return self.dm

    def avg_dist(self):
        return avg_dist(self.distance_matrix())

    def diameter(self):
        return diameter(self.distance_matrix())


if __name__ == '__main__':
    base_dir = os.path.dirname(__file__)
    data = ps.read_csv(os.path.join(base_dir, '..', 'cv03', 'KarateClub.csv'), sep=';', header=None)

    nodes_count = max(list(data[0]) + list(data[1]))
    m = np.zeros((nodes_count, nodes_count), dtype=int)
    for a, b in zip(data[0], data[1]):
        m[a - 1, b - 1] = m[b - 1, a - 1] = 1

    g = Graph(m)

    print(f'matrix =\n{m}')

    dm = g.distance_matrix()
    print(f'dist =\n{dm}')

    dia = g.diameter()
    print(f'diameter = {dia}')

    print('closeness:')
    for i in range(len(dm)):
        print(f'C({i}) = {closeness(dm, i)}')
