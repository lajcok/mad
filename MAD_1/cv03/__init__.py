import matplotlib.pyplot as plt


def degree_data(graph):
    return {
        deg: graph.count_degrees(deg)
        for deg in range(graph.max_degree() + 1)
    }


def degree_histogram(graph):
    degrees = [
        graph.degree(node)
        for node in graph.nodes
    ]
    n, bins, patches = plt.hist(
        degrees,
        bins=[deg + .5 for deg in range(graph.max_degree() + 1)],
        rwidth=.2,
    )
    plt.vlines(
        graph.avg_degree(),
        min(n), max(n),
        colors='red',
        linestyles='dashed',
    )
    plt.show()
