import os
from functools import reduce

import pandas as ps

from MAD_1.cv03 import degree_histogram, degree_data


class Graph:
    def __init__(self, nodes, edges):
        self.nodes = set(nodes)
        self.edges = set(edges)

    def __str__(self):
        return f'''G = (
    V = {self.nodes}
    E = {self.edges}
)'''

    def degree(self, node=None):
        def deg(n):
            return len([
                edge
                for edge in self.edges
                if n in edge
            ])

        return deg(node) if node is not None else {deg(nd) for nd in self.nodes}

    def min_degree(self):
        return min(
            self.degree(node)
            for node in self.nodes
        )

    def max_degree(self):
        return max(
            self.degree(node)
            for node in self.nodes
        )

    def avg_degree(self):
        degrees = [
            self.degree(node)
            for node in self.nodes
        ]
        return sum(degrees) / len(degrees)

    def get_by_degree(self, degree):
        return [
            node
            for node in self.nodes
            if self.degree(node) == degree
        ]

    def count_degree(self, degree):
        return len(self.get_by_degree(degree))

    def relative_degree(self, degree):
        return self.count_degree(degree) / len(self.nodes)


if __name__ == '__main__':
    base_dir = os.path.dirname(__file__)
    data = ps.read_csv(os.path.join(base_dir, 'KarateClub.csv'), sep=';', header=None)
    g = Graph(
        nodes=set(reduce(
            lambda a, b: a + b,
            [list(data[c]) for c in data]
        )),
        edges=set(zip(
            *tuple(
                list(data[c])
                for c in data
            ))
        ),
    )

    print(g)
    print(f'min = {g.min_degree()}; max = {g.max_degree()}; avg = {g.avg_degree()}')

    degree_histogram(g)

    counts = degree_data(g)
    output = ps.DataFrame(counts.items())
    print(f'degree counts: {output}')

    output.to_csv(os.path.join(base_dir, 'degree_count.csv'), header=('degree', 'count'))
