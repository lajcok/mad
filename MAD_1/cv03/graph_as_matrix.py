import os

import numpy as np
import pandas as ps

from MAD_1.cv03 import degree_histogram, degree_data


class Graph:
    def __init__(self, matrix):
        self.matrix = matrix
        self.nodes = {
            i + 1
            for i in range(len(matrix))
        }

    def __str__(self):
        return 'Matrix G:\n' + str(self.matrix)

    def degree(self, node):
        return sum(self.matrix[node - 1])

    def min_degree(self):
        return min(
            self.degree(i)
            for i in range(len(self.matrix))
        )

    def max_degree(self):
        return max(
            self.degree(i)
            for i in range(len(self.matrix))
        )

    def avg_degree(self):
        degrees = [
            self.degree(i)
            for i in range(len(self.matrix))
        ]
        return sum(degrees) / len(degrees)

    def count_degree(self, degree):
        return len([
            i
            for i in range(len(self.matrix))
            if self.degree(i) == degree
        ])

    def relative_degree(self, degree):
        return self.count_degree(degree) / len(self.matrix)


if __name__ == '__main__':
    base_dir = os.path.dirname(__file__)
    data = ps.read_csv(os.path.join(base_dir, 'KarateClub.csv'), sep=';', header=None)

    nodes_count = max(list(data[0]) + list(data[1]))
    m = np.zeros((nodes_count, nodes_count), dtype=int)
    for a, b in zip(data[0], data[1]):
        m[a - 1, b - 1] = m[b - 1, a - 1] = 1

    g = Graph(m)

    print(g)
    print(f'min = {g.min_degree()}; max = {g.max_degree()}; avg = {g.avg_degree()}')

    degree_histogram(g)

    counts = degree_data(g)
    output = ps.DataFrame(counts.items())
    print(f'degree counts: {output}')

    output.to_csv(os.path.join(base_dir, 'degree_count.csv'), header=('degree', 'count'))
