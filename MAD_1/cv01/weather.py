from enum import Enum


class Weather:
    class Outlook(Enum):
        Sunny = 'sunny'
        Overcast = 'overcast'
        Rainy = 'rainy'

        def __str__(self):
            return self.value

        def __eq__(self, other):
            return str(self) == str(other)

        def __hash__(self):
            return hash(str(self))

    class Temperature(Enum):
        Cool = 'cool'
        Mild = 'mild'
        Hot = 'hot'

        def __str__(self):
            return self.value

        def __eq__(self, other):
            return str(self) == str(other)

        def __hash__(self):
            return hash(str(self))

    class Humidity(Enum):
        Normal = 'normal'
        High = 'high'

        def __str__(self):
            return self.value

        def __eq__(self, other):
            return str(self) == str(other)

        def __hash__(self):
            return hash(str(self))

    class Play(Enum):
        Yes = 'yes'
        No = 'no'

        def __str__(self):
            return self.value

        def __eq__(self, other):
            return str(self) == str(other)

        def __hash__(self):
            return hash(str(self))

    def __init__(
            self,
            outlook=None,
            temperature=None,
            humidity=None,
            windy=None,
            play=None,
    ):
        self.outlook = outlook
        self.temperature = temperature
        self.humidity = humidity
        self.windy = windy
        self.play = play

    def __str__(self):
        return 'If ' + (' and '.join((
            f'{prop} = {value}'
            for prop, value in vars(self).items()
            if prop is not 'play' and value is not None
        )) or 'none') + f' then play = {self.play}'

    def __hash__(self):
        return hash(vars(self).values())

    def __eq__(self, other):
        return vars(self) == vars(other)

    def subsumes(self, conditions):
        from functools import reduce
        try:
            return reduce(lambda a, b: a and b, [
                getattr(conditions, prop) == value
                for prop, value in vars(self).items()
                if prop != 'play' and value is not None  # TODO Include 'play' in the comparison?
            ])
        except TypeError:
            return True
