from MAD_1.cv01.weather import Weather

ref_supp = 0.14
ref_conf = 1

if __name__ == '__main__':
    from MAD_1.cv01.generator import generate
    from MAD_1.cv01.loader import load

    generated = list(generate())
    nominal = list(load('data/weather_nominal.csv'))

    for g in generated:
        instances = [
            w
            for w in nominal
            if g.subsumes(w)
        ]

        r = len(instances)
        n = len(nominal)
        c_yes = len(list(filter(lambda w: str(w.play) == 'yes', instances)))
        c_no = len(list(filter(lambda w: str(w.play) == 'no', instances)))

        supp = r / n if n > 0 else '-'
        conf_g = c_yes if g.play == Weather.Play.Yes else c_no
        conf = conf_g / r if r > 0 else '-'

        # TODO Check results
        print(g)
        print(f'Occurrences: {r}, support: {supp}, confidence: {conf}')
