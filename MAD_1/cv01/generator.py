from MAD_1.cv01.weather import Weather


def generate():
    return (
        Weather(
            outlook=outlook,
            temperature=temperature,
            humidity=humidity,
            windy=windy,
            play=play,
        )
        for outlook in tuple(Weather.Outlook) + (None,)
        for temperature in tuple(Weather.Temperature) + (None,)
        for humidity in tuple(Weather.Humidity) + (None,)
        for windy in (True, False, None)
        for play in tuple(Weather.Play)
    )


if __name__ == '__main__':
    total = 0
    for weather in generate():
        print(weather)
        total += 1
    print(f'Total: {total}')
