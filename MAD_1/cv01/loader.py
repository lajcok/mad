import pandas

from MAD_1.cv01.weather import Weather


def load(path):
    pd = pandas.read_csv(path, sep=';')
    return [
        Weather(**{
            key.lower(): value
            for key, value in row._asdict().items()
        })
        for row in pd.itertuples(index=False, name='WeatherTuple')
    ]


if __name__ == '__main__':
    for weather in load('data/weather_nominal.csv'):
        print(weather)
