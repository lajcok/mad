import itertools
import random
from functools import reduce

import numpy as np

from MAD_1.cv01.loader import load
from MAD_1.cv01.weather import Weather


class NaiveBayes:
    def __init__(self, data):
        self.data = data
        self.prior = self.rel_count(self._subsumed())
        self.by_outlook = {
            outlook: self.rel_count(self._subsumed(outlook=outlook))
            for outlook in Weather.Outlook
        }
        self.by_temperature = {
            temperature: self.rel_count(self._subsumed(temperature=temperature))
            for temperature in Weather.Temperature
        }
        self.by_humidity = {
            humidity: self.rel_count(self._subsumed(humidity=humidity))
            for humidity in Weather.Humidity
        }
        self.by_windy = {
            windy: self.rel_count(self._subsumed(windy=windy))
            for windy in [True, False]
        }

    def rel_count(self, conditions):
        return {
            play: count / len(conditions)
            for play, count in self._count(conditions).items()
        }

    def _count(self, conditions):
        return {
            play: len(cs)
            for play, cs in self._classify(conditions).items()
        }

    def _classify(self, conditions):
        res = {play: [] for play in Weather.Play}
        for cond in conditions:
            res[cond.play].append(cond)
        return res

    def _subsumed(self, weather=None, **kwargs):
        ref = weather if weather is not None else Weather(**kwargs)
        return [
            w
            for w in self.data
            if ref.subsumes(w)
        ]

    def predict(self, weather=None, **kwargs):
        ref = weather if weather is not None else Weather(**kwargs)
        pm = {p: 1 for p in Weather.Play}
        mapped = [
            self.prior,
            self.by_outlook[ref.outlook] if ref.outlook is not None else pm,
            self.by_temperature[ref.temperature] if ref.temperature is not None else pm,
            self.by_humidity[ref.humidity] if ref.humidity is not None else pm,
            self.by_windy[ref.windy] if ref.windy is not None else pm,
        ]
        return {
            play: reduce(lambda a, b: a * b, map(lambda a: a[play], mapped))
            for play in Weather.Play
        }


def k_fold(data, k):
    data = random.sample(data, len(data))

    def fold_data():
        groups = {p: [] for p in Weather.Play}
        for w in data:
            groups[w.play].append(w)

        partitioned = [
            np.array_split(group, k)
            for group in groups.values()
        ]

        return [
            list(itertools.chain(*(partition[i] for partition in partitioned)))
            for i in range(k)
        ]

    def error_rate(learn_set, test_set):
        nb = NaiveBayes(learn_set)
        assess_set = [
            nb.predict(test_case)
            for test_case in test_set
        ]

        return len([
            True
            for test_case, assess in zip(test_set, assess_set)
            if reduce(lambda a, b: a if a[1] > b[1] else b, assess.items())[0] != test_case.play
        ]) / len(test_set) if len(test_set) > 0 else 0

    folds = fold_data()
    error_rates = np.array([
        error_rate(
            learn_set=list(itertools.chain(*(folds[j] for j in range(k) if j is not i))),
            test_set=folds[i],
        )
        for i in range(k)
    ])

    return error_rates.mean(), error_rates.var(), error_rates


if __name__ == '__main__':
    _data = load('weather_nominal.csv')
    _nb = NaiveBayes(_data)
    print(_nb.predict())

    for _ in range(10):
        print(k_fold(_data, 3))
