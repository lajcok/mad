import numpy as np


def gaussian_kernel(a, b, o2=1.):
    return np.exp(-np.linalg.norm(a - b) ** 2 / 2 / o2)
