import numpy as np


def epsilon_radius(similarity_matrix, epsilon):
    network = np.zeros_like(similarity_matrix)
    for i in range(len(network)):
        for j in range(i):
            if similarity_matrix[i, j] >= epsilon:
                network[(i, j), (j, i)] = similarity_matrix[i, j]
    return network


def knn(similarity_matrix, k):
    network = np.zeros_like(similarity_matrix)
    sort = np.argsort(-similarity_matrix)[:, :(k + 1)]
    for i in range(len(network)):
        for j in sort[i]:
            if i != j:
                network[i, j] = similarity_matrix[i, j]
    return network


def combo(similarity_matrix, epsilon, k):
    network = epsilon_radius(similarity_matrix, epsilon)
    missing = k - np.sum(network > 0, axis=-1)
    to_fill = [i for i, n in enumerate(missing) if n > 0]
    sort = np.argsort(-similarity_matrix[to_fill])[:, :(k + 1)]
    for i, js in zip(to_fill, sort):
        for j in js:
            if i != j:
                network[i, j] = similarity_matrix[i, j]
    return network


def nnn(similarity_matrix, k):
    network = knn(similarity_matrix, k)
    return np.minimum(network, network.transpose())
