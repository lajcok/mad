import numpy as np
import pandas as ps

from MAD_2.cv01.gaussian_kernel import gaussian_kernel
from MAD_2.cv01.network_construction import epsilon_radius, knn, combo, nnn

if __name__ == '__main__':
    data = ps.read_csv('Iris.csv', index_col='Id')
    del data['Species']

    data_matrix = np.array(list(zip(*[data[col] for col in data])))
    similarity_matrix = np.ones((len(data),) * 2, dtype=float)

    for i in range(len(data_matrix)):
        for j in range(i):
            similarity_matrix[(i, j), (j, i)] = gaussian_kernel(data_matrix[i], data_matrix[j])

    epsilon, k = .93, 3
    algorithms = {
        'epsilon': lambda m: epsilon_radius(m, epsilon),
        'knn': lambda m: knn(m, k),
        'combo': lambda m: combo(m, epsilon, k),
        'nnn': lambda m: nnn(m, k),
    }

    matrices = {
        name: fn(similarity_matrix)
        for name, fn in algorithms.items()
    }

    data_frames = {
        name: ps.DataFrame(
            data=network,
            index=data.index,
            columns=data.index,
        )
        for name, network in matrices.items()
    }

    for name, df in data_frames.items():
        df.to_csv(f'{name}.csv')
