import tkinter as tk
from time import time
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox

import pandas as ps

from MAD_1.proj_implementation.data import load, preprocess
from MAD_2.analysis.network_construction import epsilon_radius, gaussian_kernel_matrix


class Gui:
    class Loader:
        def __init__(self, gui, menu):
            self.gui = gui
            self.menu = menu
            self.menu.add_command(label='Open', command=self.open, accelerator='Ctrl+O')
            gui.root.bind_all('<Control-o>', gui.handle(self.open))
            self.menu.add_command(label='Save', command=self.save, accelerator='Ctrl+S', state=tk.DISABLED)
            gui.root.bind_all('<Control-s>', gui.handle(self.save))

        def open(self):
            path = filedialog.askopenfilename(title='Select dataset')
            if path:
                self.gui.load(path)

        def save(self):
            if self.gui.result is None:
                return

            path = filedialog.asksaveasfilename(
                title='Save result to',
                defaultextension='.csv',
            )

            if path:
                print('saving')
                self.gui.save(path)

    class DatasetOptions:
        def __init__(self, master, gui):
            self.master = master
            self.gui = gui
            self.data = None

            self.inst_n = tk.IntVar(value=0)
            inst_frame = tk.Frame(master)
            tk.Label(inst_frame, text='Instances:').pack(side=tk.LEFT)
            tk.Label(inst_frame, textvariable=self.inst_n).pack(side=tk.LEFT)
            inst_frame.pack()

            lim_frame = tk.Frame(master)
            tk.Label(lim_frame, text='Limit:').pack(side=tk.LEFT)
            self.inst_limit = tk.IntVar(value=0)
            tk.Entry(lim_frame, textvariable=self.inst_limit, width=5).pack(side=tk.LEFT)
            lim_frame.pack()

            self.attr_frame = tk.LabelFrame(master, text='Attributes')
            self.attr_frame.pack()
            self.attr_checkboxes = {}
            self.attr_vars = {}
            self.prev_button = tk.Button(
                self.attr_frame,
                text='Preview',
                command=self.preview,
            )

        def set_data(self, data):
            self.data = data
            self.inst_n.set(len(data))
            self.inst_limit.set(len(data))

            for check in self.attr_checkboxes.values():
                check.pack_forget()
            self.attr_checkboxes = {}

            for attr in data:
                self.attr_vars[attr] = tk.BooleanVar(value=False)
                self.attr_checkboxes[attr] = tk.Checkbutton(
                    self.attr_frame,
                    text=str(attr),
                    variable=self.attr_vars[attr],
                )
                self.attr_checkboxes[attr].pack(anchor='w')

            self.prev_button.pack(anchor='w')

        def preview(self):
            self.gui.data_preview.delete(*self.gui.data_preview.get_children())
            limit = self.inst_limit.get()
            attrs = [
                attr
                for attr, enabled in self.attr_vars.items()
                if enabled.get()
            ]
            cols = [c for c in attrs]
            self.gui.data_preview['columns'] = cols
            for col in cols:
                self.gui.data_preview.heading(col, text=col, anchor=tk.W)
            for i, t in zip(range(limit), zip(*[self.data[c] for c in attrs])):
                self.gui.data_preview.insert('', i, str(i), text=str(i), values=t)

    def __init__(self, root):
        self.root = root

        # Menu
        self.main_menu = tk.Menu(self.root)
        self.root.config(menu=self.main_menu)

        self.file_menu = tk.Menu(self.main_menu, tearoff=0)
        self.main_menu.add_cascade(label='File', menu=self.file_menu)

        self.loader = self.Loader(self, self.file_menu)
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Exit', command=self.root.quit, accelerator='Ctrl+Q')
        self.root.bind_all('<Control-q>', self.handle(self.root.quit))

        # Left
        left_frame = tk.Frame(root)
        left_frame.pack(side=tk.LEFT)

        # Overview
        data_frame = tk.LabelFrame(left_frame, text='Dataset')
        data_frame.pack()
        self.data_options = self.DatasetOptions(data_frame, self)

        # Options
        run_frame = tk.LabelFrame(left_frame, text='Options')
        run_frame.pack()

        tk.Label(run_frame, text='Epsilon').pack()
        self.epsilon = tk.StringVar(value='0.90')
        tk.Entry(run_frame, textvariable=self.epsilon).pack(fill=tk.X)

        self.run_btn = tk.Button(run_frame, text='Run', command=self.run, state=tk.DISABLED)
        self.run_btn.pack(fill=tk.X)

        self.result = None

        # Preview
        self.data_preview = ttk.Treeview(root)
        self.data_preview.pack(expand=True, fill=tk.BOTH)
        self.data = None

    def handle(self, action):
        def _handler(*args, **kwargs):
            return action()

        return _handler

    def load(self, path):
        self.data = preprocess(load(path))
        self.data_options.set_data(self.data)
        self.run_btn.config(state=tk.NORMAL)

    def save(self, path):
        self.result.to_csv(path)
        messagebox.showinfo('Saved', f'Successfully saved result as {path}')

    def run(self):
        eps = float(self.epsilon.get())
        limit = self.data_options.inst_limit.get()
        attrs = [
            attr
            for attr, enabled in self.data_options.attr_vars.items()
            if enabled.get()
        ]
        data_matrix = self.data.loc[range(limit), attrs].to_numpy(dtype=float)

        print('starting computation')
        start = time()

        similarity_matrix = gaussian_kernel_matrix(data_matrix)
        print('similarity matrix shape', similarity_matrix.shape)

        adj_matrix = epsilon_radius(similarity_matrix, epsilon=eps)
        print('adjacency matrix shape', adj_matrix.shape)

        duration = time() - start
        print('finished')

        self.result = ps.DataFrame(
            data=adj_matrix,
            index=self.data.index[range(limit)],
            columns=self.data.index[range(limit)],
        )
        self.loader.menu.entryconfig('Save', state=tk.NORMAL)

        report = f'''Instances total: {len(self.data)}
Instances processed: {len(self.result)}
Dimension: {len(attrs)}
Duration: {duration} s

'''

        f = open('log.txt', 'w')
        f.write(report)
        f.close()

        messagebox.showinfo('Conversion complete!', f'''
{report}

Press CTRL+S to save results.
''')


if __name__ == '__main__':
    _root = tk.Tk()
    _gui = Gui(_root)
    _root.mainloop()
