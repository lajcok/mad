from timeit import timeit

import numpy as np
import pandas as ps

from MAD_2.cv01 import nnn, knn, combo


def gaussian_kernel_matrix(pts, o2=1.):
    return np.exp(-np.linalg.norm(pts[None, :] - pts[:, None], axis=-1) ** 2 / (2 * o2))


def epsilon_radius(similarity_matrix, epsilon):
    return similarity_matrix * (similarity_matrix >= epsilon)


def main():
    data = ps.read_csv(
        # 'frogs/Frogs_MFCCs.csv',
        'avila/avila-tr.txt',
        header=None,
        # dtype={
        #     'Family': 'category',
        #     'Genus': 'category',
        #     'Species': 'category',
        #     'RecordID': 'category',
        # },
        dtype={
            10: 'category',
        },
        # nrows=5000,
    )

    data_matrix = data[range(10)].to_numpy()
    # data_matrix = data[data.columns[:22]].to_numpy()
    similarity_matrix = gaussian_kernel_matrix(data_matrix)

    print(similarity_matrix.shape)
    print(similarity_matrix)

    eps, k = .95, 2
    print('eps', eps)
    network = nnn(similarity_matrix, k=k)
    print(network.shape)
    print(network)

    print('edges:', np.sum(network[np.tri(len(network), k=-1, dtype=bool)] > 0))

    net_df = ps.DataFrame(
        data=network,
        index=data.index,
        columns=data.index,
    )
    filename = f'avila/nnn_k2_full.csv'
    print('exporting to', filename)
    net_df.to_csv(filename)


if __name__ == '__main__':
    duration = timeit(main, number=1)
    print(f'Took {duration} s')
