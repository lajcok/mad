from timeit import timeit

import numpy as np
import pandas as ps

from MAD_2.cv01 import nnn, knn, combo


def gaussian_kernel_matrix(pts, o2=1.):
    return np.exp(-np.linalg.norm(pts[None, :] - pts[:, None], axis=-1) ** 2 / (2 * o2))


def epsilon_radius(similarity_matrix, epsilon):
    return similarity_matrix * (similarity_matrix >= epsilon)


def main():
    data = ps.read_csv(
        'avila-tr.txt',
        header=None,
        dtype={10: 'category'},
        # nrows=5000,
    )

    data_matrix = data[range(10)].to_numpy()
    similarity_matrix = gaussian_kernel_matrix(data_matrix)

    print(similarity_matrix.shape)
    print(similarity_matrix)

    network = epsilon_radius(similarity_matrix, epsilon=.91)
    print(network.shape)
    print(network)

    print('edges:', np.sum(network[np.tri(len(network), k=-1, dtype=bool)] > 0))

    net_df = ps.DataFrame(
        data=network,
        index=data.index,
        columns=data.index,
    )
    net_df.to_csv('epsilon_e91_full.csv')


if __name__ == '__main__':
    duration = timeit(main, number=1)
    print(f'Took {duration} s')
