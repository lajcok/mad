import pandas as ps
import numpy as np

from MAD_2.cv06.evolving_networks import link_selection_model, copying_model

if __name__ == '__main__':
    n0, m0, t, m = 20, 40, 30, 3

    lsm = link_selection_model(n0, m0, t, m)
    lsm_df = ps.DataFrame(
        lsm,
        index=np.arange(lsm.shape[0]) + 1,
        columns=np.arange(lsm.shape[1]) + 1,
    )
    print('Link Selection Model')
    print(lsm_df)
    lsm_df.to_csv('lsm.csv')

    prob = .2, .5, .8
    cms = [
        copying_model(n0, m0, t, m, p)
        for p in prob
    ]
    cm_dfs = [
        ps.DataFrame(
            cm,
            index=np.arange(cm.shape[0]) + 1,
            columns=np.arange(cm.shape[1]) + 1,
        )
        for cm in cms
    ]
    for p, cm_df in zip(prob, cm_dfs):
        print(f'Copying model (p = {p})')
        print(cm_df)
        cm_df.to_csv(f'cm_{p}.csv')
