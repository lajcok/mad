import numpy as np


def simple_network(n, m, adjacency=None):
    """
    Creates a simple network
    :param n: Number of nodes
    :param m: Number of edges
    :param adjacency: Optional 2D matrix to begin with and mutate
    :return: Returns created / passed adjacency matrix
    """
    adjacency = adjacency if adjacency is not None else np.zeros((n, n), dtype=np.int8)
    available_spots = np.tri(n, k=-1, dtype=bool)
    initial_edges = np.random.choice(
        np.arange(available_spots.size),
        size=m,
        p=(available_spots / available_spots.sum()).flatten(),
    )
    adjacency[np.unravel_index(
        initial_edges,
        shape=available_spots.shape,
    )] = 1
    return adjacency


def link_selection_model(n0, m0, t, m=1):
    n = n0 + t
    adjacency = simple_network(n0, m0, adjacency=np.zeros((n, n), dtype=np.int8))

    for i in range(n0, n):
        adj = adjacency[:i, :i]
        edge_choice = np.random.choice(
            np.arange(adj.size),
            size=m,
            p=(adj / adj.sum()).flatten(),
        )
        js = np.unravel_index(edge_choice, adj.shape)[0]

        ijs = [i] * len(js) + list(js)
        adjacency[ijs, ijs[::-1]] = 1

    return adjacency


def copying_model(n0, m0, t, m=1, p=.5):
    n = n0 + t
    adjacency = simple_network(n0, m0, adjacency=np.zeros((n, n), dtype=np.int8))
    idx = np.arange(n)

    for i in range(n0, n):
        adj = adjacency[:i, :i]
        js = np.random.choice(idx[:i], size=m)
        rnd = np.random.rand(m) < p

        to_link, to_copy = js[rnd], js[~rnd]
        options = adj[to_copy].sum(axis=0)
        denominator = options.sum()
        to_copy_link = np.random.choice(
            idx[:i], size=len(to_copy),
            p=options / denominator if denominator > 0 else None,
        )

        all_links = list(to_link) + list(to_copy_link)
        ijs = [i] * len(all_links) + all_links
        adjacency[ijs, ijs[::-1]] = 1

    return adjacency
