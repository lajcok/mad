# Title     : SIR Simulation
# Objective : Simulate spread in SIR model and visualize

if (!require('igraph'))
  install.packages('igraph')
library(igraph)

if (!require('itertools'))
  install.packages('itertools')
library(itertools)

if (!require('shiny'))
  install.packages('shiny')
library(shiny)

simulateSIR <- function (net, I, beta, gamma) {
  # init
  V(net)$sir.state <- 'S'
  V(net)[sample(V(net), size = I, replace = TRUE)]$sir.state <- 'I'

  # helper
  random.bool <- function (size = 1, probTrue = .5)
    sample(
      c(TRUE, FALSE),
      size = size,
      replace = TRUE,
      prob = c(probTrue, 1-probTrue)
    )

  # iterator
  it <- function() {
    # classification
    infected <- V(net)$sir.state == 'I'
    susceptible <- V(net)[unlist(neighborhood(net, nodes = infected))]$sir.state == 'S'

    # recovery
    V(net)[infected][random.bool(sum(infected), gamma)]$sir.state <- 'R'
    # infection spread
    V(net)[susceptible][random.bool(sum(susceptible), beta)]$sir.state <- 'I'

    # updated
    net
  }

  # props
  obj <- list(nextElem = it)
  class(obj) <- c('simulateSIR', 'abstractiter', 'iter')
  obj
}

## init simulation
g <- graph("Zachary")
V(g)$size <- 8
V(g)$label <- ""
l <- layout.auto(g)
plot(g, layout = l)

sir <- simulateSIR(g, I = 10, beta = .1, gamma = 1/14)

## run animation
runApp(list(
  ui = fluidPage(
    mainPanel(plotOutput('distPlot'))
  ),
  server = function(input, output, session) {
    autoInvalidate <- reactiveTimer(1000, session)
    output$distPlot <- renderPlot({

      autoInvalidate()
      net <- nextElem(sir)
      V(net)$color <- sapply(V(net)$sir.state,
        function (state)
          switch(state, "S" = "blue", "I" = "red", "R" = "green")
      )

      plot(net, layout = l)

    })
  }
))
