import numpy as np
import pandas as ps
from matplotlib import pyplot as plt

from MAD_2.cv02.network_clustering import cosine_similarity, count_degrees, common_neighbors, HierarchicalClustering

if __name__ == '__main__':
    # loading
    data = ps.read_csv('KarateClub.csv', sep=';', header=None)
    max_id = max(list(data[0]) + list(data[1]))

    # init
    adjacency_matrix = np.zeros((max_id,) * 2, dtype=bool)
    for a, b in zip(data[0], data[1]):
        i, j = a - 1, b - 1
        adjacency_matrix[(i, j), (j, i)] = 1

    common = common_neighbors(adjacency_matrix)
    degrees = count_degrees(adjacency_matrix)
    similarity_matrix = cosine_similarity(common, degrees)

    # clustering
    hc = HierarchicalClustering(similarity_matrix)

    # heat map
    plt.imshow(hc.similarity_matrix)
    plt.show()

    # iteration
    for i, step in enumerate(hc):
        print(i, step.clusters)
        if len(step.clusters) <= 3:
            break
