import numpy as np


def common_neighbors(adjacency_matrix):
    return np.dot(*[adjacency_matrix.astype(int)] * 2)


def count_degrees(adjacency_matrix):
    return np.sum(adjacency_matrix > 0, axis=-1)


def cosine_similarity(common, degrees):
    return common / np.sqrt(np.outer(degrees, degrees.transpose()))


def average_linkage(similarity_matrix, clusters):
    def _compare_clusters(*abc):
        dim_masks = [np.zeros(len(similarity_matrix), dtype=bool) for _ in range(len(abc))]
        for i, a in enumerate(abc):
            dim_masks[i][a] = True
        intersection = similarity_matrix[np.outer(* dim_masks)]
        return np.mean(intersection)

    return np.array([
        [
            _compare_clusters(a, b)
            for b in clusters
        ]
        for a in clusters
    ])


class HierarchicalClustering:

    def __init__(self, similarity_matrix, clusters=None, cluster_similarity=None):
        self.similarity_matrix = similarity_matrix
        # TODO store clusters as a vector to improve performance
        self.clusters = [[i] for i in range(len(similarity_matrix))] if clusters is None else clusters
        self.cluster_similarity = similarity_matrix if cluster_similarity is None else cluster_similarity

    def step(self):
        non_diagonal = self.cluster_similarity.copy()
        np.fill_diagonal(non_diagonal, -np.inf)
        i, j = np.unravel_index(non_diagonal.argmax(), non_diagonal.shape)

        new_cluster = self.clusters[i] + self.clusters[j]
        next_clusters = [
            c if ci != i else new_cluster
            for ci, c in enumerate(self.clusters)
            if ci != j
        ]

        cluster_similarity = average_linkage(self.similarity_matrix, next_clusters)

        return self.__class__(
            similarity_matrix=self.similarity_matrix,
            clusters=next_clusters,
            cluster_similarity=cluster_similarity,
        )

    def __next__(self):
        return self.step()

    def __iter__(self):
        step = self
        while len(step.clusters) >= 1:
            yield step
            step = next(step)
