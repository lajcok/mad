import pandas as ps

from MAD_2.cv05.community_network_models import holme_kim_model, bianconi_model

if __name__ == '__main__':
    node_count = 100

    # Holme & Kim model
    hk = holme_kim_model(
        n0=10,
        m=2,
        t=node_count - 10,
        triad_formation=True,
    )
    hk_df = ps.DataFrame(hk, index=range(hk.shape[0]), columns=range(hk.shape[1]))
    # hk_df.to_csv('holme_kim.csv')

    # Bianconi model
    b = bianconi_model(
        n0=10,
        m0=10,
        t=node_count - 10,
        m=3,
        p=.5,
    )
    b_df = ps.DataFrame(b, index=range(b.shape[0]), columns=range(b.shape[1]))
    # b_df.to_csv('bianconi.csv')

    # Bianconi model (accelerated)
    ba = bianconi_model(
        n0=10,
        m0=10,
        t=node_count - 10,
        m=3,
        p=.5,
        theta=.2,
    )
    ba_df = ps.DataFrame(ba, index=range(ba.shape[0]), columns=range(ba.shape[1]))
    # b_df.to_csv('bianconi_accelerated.csv')
