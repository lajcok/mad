import warnings

import numpy as np


def holme_kim_model(n0, m, t, triad_formation=True):
    """
    Holme & Kim community network model
    :param n0: Number of nodes in initial network
    :param m: Edges to add at every `t`
    :param t: Number of iterations
    :param triad_formation: Whether triad formation will be applied
    :return: Returns 2D array of shape `(a,a)` where `a = n0 + t`
    """
    n = n0 + t
    adjacency = np.zeros((n, n), dtype=np.int8)
    idx = np.arange(n)

    for i in range(max(n0, 1), n):
        # preferential attachment
        deg = np.sum(adjacency[:i, :i], axis=0) + 1
        js = np.random.choice(idx[:i], size=m, p=deg / deg.sum())

        ijs = np.hstack(([i] * len(js), js))
        adjacency[ijs, ijs[::-1]] = 1

        if triad_formation:
            for j, neighbors in zip(js, adjacency[js, :i]):
                bias = neighbors * (1 - adjacency[i, :i])
                norm = bias.sum()

                if norm > 0:
                    # actual triad formation
                    w = np.random.choice(idx[:i], p=bias / norm)
                else:
                    # fallback to preferential attachment
                    deg = np.sum(adjacency[:i, :i], axis=0) + 1
                    w = np.random.choice(idx[:i], p=deg / deg.sum())

                adjacency[(i, w), (w, i)] = 1

    return adjacency


def bianconi_model(n0, m0, t, m=2, p=.5, theta=0):
    """
    Bianconi community network model
    :param n0: Number of nodes in initial network
    :param m0: Number of edges in initial network
    :param t: Number of iterations
    :param m: Edges to add at every `t`
    :param p: Probability of triad formation, in the other case random attachment will apply
    :param theta: Exponent parameter used for accelerated growth, that is, number of edges added in every iteration
                  Applied as follows `mi = m * i ** theta`, `mi` - edges to be added in iteration, `i` - iteration index
                  Defaults to `0` - accelerated growth is disabled
    :return: Returns 2D array of shape `(a,a)` where `a = n0 + t`
    """
    # simple connected network
    n = n0 + t
    adjacency = np.zeros((n, n), dtype=np.int8)
    adjacency[:n0, :n0] += holme_kim_model(0, int(m0 / n0), n0, triad_formation=False)
    idx = np.arange(n)

    for i in range(n0, n):
        # accelerated growth
        mi = m * i ** theta

        # first link
        i1 = np.random.choice(idx[:i])
        adjacency[(i, i1), (i1, i)] = 1

        # other links
        for _ in range(1, round(mi)):
            # triad formation or random attachment
            options = adjacency[i1, :i] if np.random.random() < p else 1 - adjacency[i, :i]
            denominator = options.sum()
            i2 = np.random.choice(idx[:i], p=options / denominator) if denominator > 0 else None

            if i2 is not None:
                adjacency[(i, i2), (i2, i)] = 1
            else:
                warnings.warn('The network might be over-saturated! Too many links to be added.')

    return adjacency
