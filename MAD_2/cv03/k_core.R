library('igraph')

edges <- read.csv('KarateClub.csv', sep = ';')

net <- graph_from_data_frame(edges, directed = FALSE)
plot(net)

core <- coreness(net, mode = 'all')
barplot(core, cex.names = .8)

c <- cliques(net, min = 3, max = 5)
mc <- max_cliques(net, min = 3, max = 5)
