import numpy as np
import pandas as ps

from MAD_2.cv03.k_core import k_core, count_degrees

if __name__ == '__main__':
    # loading
    data = ps.read_csv('KarateClub.csv', sep=';', header=None)
    max_id = max(list(data[0]) + list(data[1]))

    # init
    adjacency_matrix = np.zeros((max_id,) * 2, dtype=bool)
    for a, b in zip(data[0], data[1]):
        i, j = a - 1, b - 1
        adjacency_matrix[(i, j), (j, i)] = 1

    # computation
    degrees = count_degrees(adjacency_matrix)
    cores = {
        i + 1: k_core(i + 1, adjacency_matrix, degrees) + 1
        for i in range(4)
    }
