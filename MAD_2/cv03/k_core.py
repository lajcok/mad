import numpy as np


def common_neighbors(adjacency_matrix):
    return np.dot(*[adjacency_matrix.astype(int)] * 2)


def count_degrees(adjacency_matrix):
    return np.sum(adjacency_matrix > 0, axis=-1)


def k_core(k, adjacency_matrix, degrees):
    k_core_mask = degrees >= k
    at_least_k = count_degrees(adjacency_matrix[:, k_core_mask]) >= k

    while np.sum(at_least_k) < np.sum(k_core_mask):
        k_core_mask = at_least_k
        at_least_k = count_degrees(adjacency_matrix[:, k_core_mask]) >= k

    return np.arange(len(adjacency_matrix))[k_core_mask]
