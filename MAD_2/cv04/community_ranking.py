import numpy as np


def average_weighted_precision(weight_matrix, community_matrix):
    w_pos = np.sum(weight_matrix * community_matrix, axis=-1)
    w_all = np.sum(weight_matrix, axis=-1)
    w = np.divide(w_pos, w_all, out=np.zeros_like(w_all), where=w_all != 0)
    return w.mean()


def average_precision(weight_matrix, community_matrix):
    w_in_cluster = np.sum(weight_matrix * community_matrix, axis=-1)
    w_out_cluster = np.sum(weight_matrix * ~community_matrix, axis=-1)
    p = w_in_cluster >= w_out_cluster
    return p.mean()


def modularity(weight_matrix, community_matrix):
    edges_2 = 2 * np.sum(weight_matrix > 0)
    degrees = np.sum(weight_matrix > 0, axis=-1)
    return np.sum(weight_matrix - np.outer(degrees, degrees.T) * community_matrix / edges_2) / (2 * edges_2)
