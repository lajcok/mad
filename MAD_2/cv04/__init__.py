import numpy as np
import pandas as ps

from MAD_2.cv04.community_ranking import modularity, average_weighted_precision, average_precision

if __name__ == '__main__':
    iris_data = ps.read_csv('Iris.csv', index_col='Id')
    iris_nets = {
        method: ps.read_csv(f'{method}.csv', index_col=0).values
        for method in ['epsilon', 'knn', 'combo']
    }

    species_vector = iris_data['Species']
    species_neighbors = np.ones([len(iris_data)] * 2, dtype=bool)
    for i, di in enumerate(iris_data.index):
        for j, dj in zip(range(i), iris_data.index):
            species_neighbors[(i, j), (j, i)] = species_vector[di] == species_vector[dj]

    rankings = ps.DataFrame(
        data={
            rank_method.__name__: [
                rank_method(net_matrix, species_neighbors)
                for net_matrix in iris_nets.values()
            ]
            for rank_method in [average_precision, average_weighted_precision, modularity]
        },
        index=iris_nets.keys(),
    )
